#!/bin/sh
mkdir -p debian/tmp/usr/doc/topkg
mkdir -p debian/tmp$OCAML_STDLIB_DIR/topkg
install --mode 644 _build/CHANGES.md debian/tmp/usr/doc/topkg/CHANGES.md
install --mode 644 _build/LICENSE.md debian/tmp/usr/doc/topkg/LICENSE.md
install --mode 644 _build/README.md debian/tmp/usr/doc/topkg/README.md
install --mode 644 _build/pkg/META debian/tmp$OCAML_STDLIB_DIR/topkg/META
install --mode 644 _build/src/topkg.cma debian/tmp$OCAML_STDLIB_DIR/topkg/topkg.cma
install --mode 644 _build/src/topkg.cmi debian/tmp$OCAML_STDLIB_DIR/topkg/topkg.cmi
if [ -f _build/src/topkg.cmti ]; then install --mode 644 _build/src/topkg.cmti debian/tmp$OCAML_STDLIB_DIR/topkg/topkg.cmti; fi
install --mode 644 _build/src/topkg.mli debian/tmp$OCAML_STDLIB_DIR/topkg/topkg.mli
install --mode 644 _build/topkg.opam debian/tmp$OCAML_STDLIB_DIR/topkg/opam
